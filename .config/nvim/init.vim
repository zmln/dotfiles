call plug#begin('~/.local/share/nvim/plugged')
Plug 'dracula/vim'
Plug 'SirVer/ultisnips'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'vimwiki/vimwiki'
Plug 'itchyny/lightline.vim'

"Javascript
Plug 'pangloss/vim-javascript'
Plug 'carlitux/deoplete-ternjs'
Plug 'ternjs/tern_for_vim', { 'do': 'npm install' }

"TypeScript
Plug 'HerringtonDarkholme/yats.vim'
Plug 'mhartington/nvim-typescript', { 'do': ':UpdateRemotePlugins' }
call plug#end()

set background=dark
colorscheme dracula
filetype plugin on
filetype indent on
syntax enable

set encoding=utf-8
scriptencoding utf-8
set ffs=unix,dos,mac

let mapleader=","
let g:mapleader=","

" Visuals
set number
set ruler
set mat=2
set so=8
set cmdheight=2
" set foldcolumn=1

" Movement
set backspace=eol,start,indent
set whichwrap+=<,>,h,l
set wrap

" Searching
set ignorecase
set smartcase
set hlsearch
set incsearch
set showmatch

" Remove noise
set noerrorbells
set novisualbell

" Prevent unnecessary files
set nobackup
set nowb
set noswapfile

" Tab
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4
set softtabstop=4

autocmd Filetype javascript setlocal ts=2 sts=2 sw=2 tw=79 expandtab
autocmd Filetype typescript setlocal ts=2 sts=2 sw=2 tw=79 expandtab
autocmd Filetype json setlocal ts=2 sts=2 sw=2 tw=79 expandtab

" Snippets
let g:UltiSnipsSnippetDirectories=["~/.local/share/nvim/snippets"]
let g:UltiSnipsExpandTrigger='<C-j>'
let g:UltiSnipsJumpForwardTrigger='<C-j>'
let g:UltiSnipsJumpBackwardTrigger='<C-k>'

" Autocomplete
let g:deoplete#enable_at_startup=1
let g:deoplete#omni#functions={}

autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"

if exists('g:plugs["tern_for_vim"]')
    let g:tern#command=[
    \   '/home/zi/.nvm/versions/node/v7.10.0/bin/node',
    \   '/home/zi/.local/share/nvim/plugged/tern_for_vim/node_modules/.bin/tern'
    \ ]

    let g:tern#arguments=['--persistent']
endif

" lightline
let g:lightline = {
\   'colorscheme': 'darcula',
\   'component': {
\       'readonly': '%{&readonly?"\uf023":""}',
\       'modified': '%{&modified?"\ue701":""}'
\   },
\   'component_function': {
\       'filetype': 'LightlineFileType'
\   },
\   'active': {
\       'left': [['mode', 'paste'], ['filename', 'readonly', 'modified']],
\       'right': [['lineinfo'], ['filetype']]
\   },
\   'separator': { 'left': "", 'right': "" },
\   'subseparator': { 'left': "", 'right': "" }
\ }

function! LightlineFileType()
    return &filetype == 'vim' ? "\ue7c5" :
\          &filetype == 'typescript' ? "\ue628" :
\          &filetype == 'javascript' ? "\ue74e" :
\          &filetype == 'python' ? "\ue73c" :
\          &filetype == 'markdown' ? "\ue73e" :
\          &filetype == 'html' ? "\ue736" :
\          &filetype == 'css' ? "\ue749" :
\          &filetype == 'go' ? "\ue724" :
\          &filetype == 'java' ? "\ue738" :
\          &filetype == 'ruby' ? "\ue739" :
\          &filetype
endfunction

